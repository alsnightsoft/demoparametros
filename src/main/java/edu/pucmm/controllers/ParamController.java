package edu.pucmm.controllers;

import edu.pucmm.routes.Data;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ParamController {

    @RequestMapping("/")
    public String index(@RequestParam(value = "id", required = false) String ID) {
        return process(ID);
    }

    @RequestMapping(value = "/demo", method = RequestMethod.POST)
    public String demo(@RequestParam(value = "id", required = false) String ID) {
        return process(ID);
    }

    private String process(String ID) {
        try {
            if (ID != null && !ID.isEmpty()) {
                return Data.frutas().get(Integer.parseInt(ID));
            }
            StringBuilder result = new StringBuilder();
            List<String> fruits = Data.frutas();
            for (int i = 0; i < fruits.size(); i++) {
                result.append("<br>" + "Indice: ").append(i).append(" -- Fruta: ").append(fruits.get(i));
            }
            return result.toString();
        } catch (Exception ignored) {
            return "ID no valido";
        }
    }
}
