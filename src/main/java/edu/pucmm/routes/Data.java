package edu.pucmm.routes;

import edu.pucmm.model.RouteBase;
import spark.Request;
import spark.Response;

import java.util.ArrayList;
import java.util.List;

public class Data extends RouteBase {

    public Data() {
        super("/");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        String result = "";
        List<String> fruits = frutas();
        for (int i =0; i < fruits.size(); i++) {
            result += "<br>" + "Indice: " + i + " -- Fruta: " + fruits.get(i);
        }
        return result;
    }

    public static List<String> frutas() {
        List<String> fruits = new ArrayList<>();
        fruits.add("Cereza");
        fruits.add("Manzana");
        fruits.add("Mango");
        fruits.add("Limón");
        return fruits;
    }
}
